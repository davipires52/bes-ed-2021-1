package br.ucsal.Ed20211.tad.livros;

public interface BooksSet {
		
	public void addNames (String name);
	public void addPublishers (String publisher);
	public void addDate (int date);
	
}
