package br.ucsal.Ed20211.tad.rationalNumbers;

public class RationalNumberImplement implements RationalNumbersSet{

	private int number1;
	private int number2;
	private double rational1;
	private double rational2;
	
	@Override
	public double createRational(int number1, int number2) {
		
		return number1/number2;
	}

	@Override
	public void sumRational(double rational1, double rational2) {
		
		System.out.println(rational1+rational2);
		
	}

	@Override
	public void multiplyRational(double rational1, double rational2) {

		System.out.println(rational1*rational2);
		
	}

	@Override
	public boolean equalityTest(double rational1, double rational2) {
		if(rational1 == rational2 ) {
			return true;
		}
		return false;
	}

}
