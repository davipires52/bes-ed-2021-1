package br.ucsal.Ed20211.tad.rationalNumbers;

public interface RationalNumbersSet {

	public double createRational (int number1, int number2);
	public void sumRational (double rational1, double rational2);
	public void multiplyRational (double rational1, double rational2);
	public boolean equalityTest (double rational1, double rational2);
	
	
}
