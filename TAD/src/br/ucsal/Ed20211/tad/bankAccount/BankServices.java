package br.ucsal.Ed20211.tad.bankAccount;

public class BankServices implements BankAccount{
	
	double balance;

	public BankServices() {
		
	}
	
	public BankServices(double balance) {
		this.balance = balance;
	}

	@Override
	public void showBalance() {
		
		System.out.println(balance);
		
	}

	@Override
	public void deposit(double value) {

		balance+=value;
		
	}

	@Override
	public void withdraw(double value) {


		balance -= value;
		
	}

	@Override
	public void update() {

		System.out.println(balance);
		
	}
	
	
	
}
