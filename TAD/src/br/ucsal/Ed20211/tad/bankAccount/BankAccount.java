package br.ucsal.Ed20211.tad.bankAccount;

public interface BankAccount {
	
	public void showBalance ();
	public void update();
	public void deposit (double value);
	public void withdraw (double value);
}
